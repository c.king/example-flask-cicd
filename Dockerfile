FROM python:3.9-alpine

RUN pip3 install pipenv

WORKDIR /home/app

COPY Pipfile ./

RUN set -ex && pipenv install --deploy --system

COPY . .

ENV FLASK_APP app/app.py

RUN adduser -D app
RUN chown -R app:app ./
USER app

EXPOSE 8000
ENTRYPOINT ["./gunicorn_run.sh"]
