#!/bin/sh
echo "---------------------------------"
echo " - Starting Gunicorn -"
echo "---------------------------------"

gunicorn -b0.0.0.0:8000 'app.app:create_app()'
